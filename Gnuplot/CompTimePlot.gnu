# Clear previous settings
unset  log
unset  label

# Set basic graph attributes
set    xtic       1
set    ytic       auto
set    title     "Compilation Time per num of iterations"
set    xlabel    "Num of iterations"
set    ylabel    "Compilation Time (secs)"
set    terminal   pngcairo enhanced font "arial,11"
set    output    "Plots/comp_time_graph.png"

set style data histogram 
set style fill solid border -1

# Plot data in files *.dat
plot  "Dataset/TIMES/avrora_times.dat"      using 3:xticlabels(1) title "avrora",     \
      "Dataset/TIMES/batik_times.dat"       using 3:xticlabels(1) title "batik",      \
      "Dataset/TIMES/eclipse_times.dat"     using 3:xticlabels(1) title "eclipse",    \
      "Dataset/TIMES/fop_times.dat"         using 3:xticlabels(1) title "fop",        \
      "Dataset/TIMES/h2_times.dat"          using 3:xticlabels(1) title "h2",         \
      "Dataset/TIMES/jython_times.dat"      using 3:xticlabels(1) title "jython",     \
      "Dataset/TIMES/luindex_times.dat"     using 3:xticlabels(1) title "luindex",    \
      "Dataset/TIMES/lusearch_times.dat"    using 3:xticlabels(1) title "lusearch",   \
      "Dataset/TIMES/pmd_times.dat"         using 3:xticlabels(1) title "pmd",        \
      "Dataset/TIMES/sunflow_times.dat"     using 3:xticlabels(1) title "sunflow",    \
      "Dataset/TIMES/tomcat_times.dat"      using 3:xticlabels(1) title "tomcat",     \
      "Dataset/TIMES/tradebeans_times.dat"  using 3:xticlabels(1) title "tradebeans", \
      "Dataset/TIMES/tradesoap_times.dat"   using 3:xticlabels(1) title "tradesoap",  \
      "Dataset/TIMES/xalan_times.dat"       using 3:xticlabels(1) title "xalan"
