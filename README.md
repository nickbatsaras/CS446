# Project structure
There are 3 branches in the repository:
- master
- maxine
- hotspot

## Master branch
In the master branch you will find an updated version of the project's
presentation and report. Everything else in master is outdated !

## Maxine & Hotspot branches
In each of these branches, you will find the latest version of the scripts and
dataset used to produce the report and presentation. So if what you are looking
for is the reproduction of our results, checkout one of these.

## Source Tree
- Dataset  
	Contains the output of the scripts. Specifically, .dat files hold the
	metrics we extracted from the VMs' output
- Gnuplot  
	Contains the gnuplot scripts we used to visualize the dataset files
- Scripts  
	Contains the shell scripts we used to extract the metrics of interest,
	timegc.sh and perf.sh. In also contains run.sh that clears the dataset
	folder and runs the previous scripts to get an updated dataset.
	Finally, plot.sh is used to produce all the plots we examined during
	our project
- Readings  
	Contains some of the interesting online material we found that helped
	us understand various topics

Note: In order to run our scripts you need to do it from the base directory:
```
./Scripts/timegc.sh
```

You may also need to change some paths inside the scripts (sorry !)

# Reproduction
If you are interested to reproduce our results you will need a working
installation of the following software:
- Maxine VM (2.3.0)
- Hotspot VM (1.8.0_171)
- Dacapo (9.12-MR1)
- Gnuplot

In the report you can find the specs of the platform we used to run our
experiments.

# Contact
If you come across any issue, please contact us at:
kalaentzis,batsaras{@csd.uoc.gr}, or if you feel adventurous enough, open a
gitlab issue !
